<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $primaryKey = 'officeCode';

   protected $fillable = ['city', 'adressLine1','state','country'];

   protected $hidden = [''];
}
