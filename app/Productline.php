<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productline extends Model
{
   protected $primaryKey = 'productLine';

   protected $fillable = ['textDescription', 'htmlDescription','image'];

   protected $hidden = [''];
}
