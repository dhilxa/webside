<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $primaryKey = 'customerNumber';

   protected $fillable = ['customerNumber', 'customerName'];

   protected $hidden = [''];
}
