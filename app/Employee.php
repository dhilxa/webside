<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $primaryKey = 'employeeNumber';

   protected $fillable = ['firstName', 'jobTitle'];

   protected $hidden = [''];
}
